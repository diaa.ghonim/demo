export class KeenTrackingService {
  constructor({ projectId, writeKey }) {
    this.trackingClient = new KeenTracking({ projectId, writeKey });
  }

  async recordEvent(eventName, eventBody) {
    if (this.isUserAgent) {
      eventBody = {
        ...eventBody,
        user_agent: "${keen.user_agent}",
        keen: {
          addons: [
            {
              name: "keen:ua_parser",
              input: {
                ua_string: "user_agent",
              },
              output: "parsed_user_agent",
            },
          ],
        },
      };
    }
    try {
      const response = await this.trackingClient.recordEvent(
        eventName,
        eventBody
      );
      const data = await response.clone().json();
      console.log(data, "from keen class service" + eventName);
      this.resetAllData();
    } catch (error) {
      // handle errors
      console.log("failure", error);
      this.resetAllData();
    }
  }

  initAutoTracking() {
    this.trackingClient.initAutoTracking();
  }

  setIsUserAgent(value) {
    this.isUserAgent = value;
  }

  setIsGeoData(value) {
    this.isGeoData = isGeoData;
  }

  setIsAllData(value) {
    this.isAllData = value;
  }

  resetAllData() {
    this.isUserData = false;
    this.isGeoData = false;
    this.IsAllData = false;
  }
}
