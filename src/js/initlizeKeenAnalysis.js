import { chart, chart2, chart3 } from "./initlizeDataVisulaziation.js";
// Configure a client instance for your project
const clientAnalysis = new KeenAnalysis({
  projectId: "610abf2a86f1d36014129f33",
  readKey:
    "bdfe24410bc576d2aca600c2c544c7b5e5206ad27516b6957276b5037bc4014aa8bdcaf77635959eb251bf5126e097b946bf445203875444e9d51ad3d5daab5f581e6e5376f1a5d59fd73718e044c02c34fb908e0d296e04d37f8b0445714fa7",
  // masterKey: "751B9CF78A160321E94ACC2519C4F272E0FD5133850906452AD2551FFF9D93B9",
});
console.log(clientAnalysis.masterKey());
const chart4 = new KeenDataviz({
  container: "#line",
  type: "line",
  title: "Mobile purchases",
  stacking: "normal",
  widget: {
    legend: {
      // type: "column",
      // enabled: true,
      // position: "left",
      // layout: "horizontal",
      // alignments: "center",
      enabled: true,
      position: "left",
      alignment: "center",
      layout: "vertical",
    },
    // axisY: {
    //   tickPadding: 13,
    //   title: "axis y title",
    //   enabled: true,
    //   padding: 20,
    // },
    // axisX: {
    //   tickPadding: 13,
    //   title: "axis y title",
    //   enabled: true,
    //   padding: 20,
    // },
    title: {
      content: "Fruites purchases details",
    },
    subtitle: {
      content: "Daily results",
    },
    strokeWidth: 5,
  },

  // widget: {
  //   theme: {
  //     axisY: {
  //       tickPadding: 13,
  //       title: "axis y title",
  //       enabled: true,
  //       padding: 20,
  //     },
  //   },
  // },
  settings: {
    groupMode: "stacked",

    // margins: {
    //   top: 20,
    //   left: 45,
    //   right: 45,
    //   bottom: 30,
    // },
    curve: "spline",
  },
  // labelMapping: {
  //   apps: "Apps",
  //   books: "Books",
  //   games: "Games",
  //   sounds: "Sounds",
  // },
  renderOnVisible: true,
  palette: "modern",
});
const chart5 = new KeenDataviz({
  container: "#pie",
  // type: "table",
  title: "Mobile purchases",
  type: "metric",
  type: "metric",
  settings: {
    valuePrefix: "$",
    formatValue: "${number; 0.0a}",

    type: "difference",
    usePercentDifference: true,
  },
  // stacking: "normal",
  // legend: {
  //   position: "bottom",
  // },
  // labelMapping: {
  //   apps: "orange",
  //   books: "lemon",
  //   games: "avocado",
  //   sounds: "mango",
  // },
  renderOnVisible: true,
  palette: "modern",
  choropleth: {
    map: "us",
  },
});

//
document.getElementById("delete-event").addEventListener("click", function (e) {
  clientAnalysis
    .del({
      url: clientAnalysis.url("events", "logins"),
      api_key: clientAnalysis.masterKey(),
    })
    .then((res) => {
      // Handle response
      console.log(response, "response after delete event");
    })
    .catch((err) => {
      // Handle error
      console.log(err, "err after delete event");
    });
});
clientAnalysis
  .get({
    url: clientAnalysis.url("events", "logins", "properties", "keen.id"),
    api_key: clientAnalysis.readKey(),
  })
  .then((res) => {
    // Handle response
    // console.log(res, "get events");
  })
  .catch((err) => {
    // Handle error
    console.log(err, "get events");
  });
async function queryEventForAnalysis() {
  try {
    const countOfPageViews = await clientAnalysis.query({
      analysis_type: "count",
      event_collection: "purchases",
      timeframe: "this_10_days",
      // timeframe: {
      //   start: "2021-03-20T00:00:00.000-00:00",
      //   end: "2021-10-26T00:00:00.000-00:00",
      // },
      group_by: ["item"],

      interval: "daily",
    });
    console.log(countOfPageViews, "hshhs");
    chart4.render(countOfPageViews);
    const countOfPageViewsPie = await clientAnalysis.query({
      analysis_type: "count",
      event_collection: "pageviews",
      timeframe: "this_month",
      // timeframe: {
      //   start: "2021-03-20T00:00:00.000-00:00",
      //   end: "2021-10-26T00:00:00.000-00:00",
      // },
      filters: [
        {
          operator: "exists",
          property_type: String,
          property_value: true,
          property_name: "geo.country",
        },
      ],
      group_by: ["geo.country"],

      interval: "daily",
    });
    console.log(countOfPageViewsPie, "countOfPageViewsPie");
    chart5.render(countOfPageViewsPie);
    const countViewsUniques = await clientAnalysis.query({
      analysisType: "count_unique",
      eventCollection: "pageviews",
      targetProperty: "user.uuid",
      timeframe: "this_10_days",
      group_by: ["keen.created_at"],

      interval: "daily",
    });
    chart2.render(countViewsUniques);
    // console.log(countViewsUniques, "countViewsUnique");
    const minimumPropValue = await clientAnalysis.query({
      analysisType: "minimum",
      eventCollection: "purchases",
      targetProperty: "price",
      timeframe: "this_7_days",
      // interval: "daily",
    });
    console.log(minimumPropValue, "minimumPropValue");

    const maximumPropValue = await clientAnalysis.query({
      analysisType: "maximum",
      eventCollection: "purchases",
      targetProperty: "price",
      timeframe: "this_7_days",
      // interval: "daily",
    });
    // console.log(maximumPropValue, "maximumPropValue");

    const sumPropValue = await clientAnalysis.query({
      analysisType: "sum",
      eventCollection: "purchases",
      targetProperty: "price",
      timeframe: "this_7_days",
      // interval: "daily",
    });
    // console.log(sumPropValue, "sumPropValue");

    const averagePropValue = await clientAnalysis.query({
      analysisType: "average",
      eventCollection: "purchases",
      targetProperty: "price",
      timeframe: "this_7_days",
      // interval: "daily",
    });
    // console.log(averagePropValue, "averagePropValue");

    const medianPropValue = await clientAnalysis.query({
      analysisType: "median",
      eventCollection: "purchases",
      targetProperty: "price",
      timeframe: "this_7_days",
      // interval: "daily",
    });
    // console.log(medianPropValue, "medianPropValue");

    const percentilePropValue = await clientAnalysis.query({
      analysisType: "percentile",
      eventCollection: "purchases",
      targetProperty: "price",
      timeframe: "this_7_days",
      percentile: 90,
      // interval: "daily",
    });
    // console.log(percentilePropValue, "percentilePropValue");

    const selectUniquePropValue = await clientAnalysis.query({
      analysisType: "select_unique",
      eventCollection: "purchases",
      targetProperty: "user.name",
      timeframe: "this_7_days",
      filters: [
        {
          operator: "exists",
          property_type: String,
          property_value: true,
          property_name: "user.name",
        },
      ],

      // interval: "daily",
    });
    // console.log(selectUniquePropValue, "selectUniquePropValue");

    const standardDeviationPropValue = await clientAnalysis.query({
      analysisType: "standard_deviation",
      eventCollection: "purchases",
      targetProperty: "price",
      timeframe: "this_7_days",

      // interval: "daily",
    });
    // console.log(standardDeviationPropValue, "standardDeviationPropValue");
    // standard_deviation

    const multiAnalysis = await clientAnalysis.query({
      analysisType: "multi_analysis",
      eventCollection: "pageviews",
      analyses: {
        "unique users": {
          analysisType: "count_unique",
          targetProperty: "user.uuid",
        },
        "total visits": {
          analysisType: "count",
        },
      },
      timeframe: "this_7_days",
      // interval: "daily",
    });
    // console.log(multiAnalysis, "multiAnalysis");
    // chart3.render(multiAnalysis);

    const funnel = await clientAnalysis.query({
      analysisType: "funnel",
      steps: [
        {
          eventCollection: "pageviews",
          actorProperty: "user.uuid",
          timeframe: "this_7_days",
          interval: "daily",
          with_actors: true,
        },
        {
          eventCollection: "clicks",
          actorProperty: "user.uuid", // = visitor.uuid from the step above
          timeframe: "this_7_days",
          with_actors: true,
          interval: "daily",
        },
        {
          eventCollection: "form_submissions",
          actorProperty: "user.uuid",
          timeframe: "this_7_days",
          with_actors: true,
          interval: "daily",
        },
      ],
    });
    chart3.render(funnel);
    // console.log(funnel, "funnel");

    const queryWithOrderBy = await clientAnalysis.query({
      analysisType: "count",
      eventCollection: "purchases",
      groupBy: "user.name",
      orderBy: { property_name: "result", direction: "DESC" },
      filters: [
        {
          property_name: "user.name",
          operator: "exists",
          property_value: true,
        },
      ],
      timeframe: "this_7_days",
    });

    // console.log(queryWithOrderBy, "queryWithOrderBy");
  } catch (err) {
    // console.log("Error", err);
  }
}

export { queryEventForAnalysis, clientAnalysis };
