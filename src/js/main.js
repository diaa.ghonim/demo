import { recordEvent } from "./initilizeKeenEvents.js";
import { queryEventForAnalysis } from "./initlizeKeenAnalysis.js";
import { KeenTrackingService } from "./KeenTrackingService.js";
import "./test.js";
const trackingClient = new KeenTrackingService({
  projectId: "610abf2a86f1d36014129f33",
  writeKey:
    "9baf98244913f6ad964dfed0fb2acb1ea927a0593494dfbad7f75ec5b53c911ccd76650d120ac658cd56f26044a5f70bcb869005b481461a593759e9a16fa0c664a24d9ed384fb2ee39a6f47e9d05ab7690ce743cb29fb5622fee7ac97d6436b",
});
document.body.addEventListener("click", () => {
  console.log("body clicked");
  trackingClient.setIsUserAgent(true);
  trackingClient.recordEvent("purchases", {
    item: "mango",
    price: 330,
    number_of_items: 50,
    user: {
      name: "diaa gonim",
    },
  });
});

let fruits = ["mango", "lemon", "apple", "orange"];
setInterval(() => {
  trackingClient.recordEvent("purchases", {
    item: fruits[Math.floor(Math.random() * 4)],
    price: Math.ceil(Math.random() * 100),
    number_of_items: Math.ceil(Math.random() * 100),
    user: {
      name: "diaa",
    },
  });
}, 1000);

setInterval(() => {
  trackingClient.recordEvent("purchases", {
    item: "apple",
    price: Math.ceil(Math.random() * 100),
    number_of_items: Math.ceil(Math.random() * 100),
    user: {
      name: "diaa",
    },
  });
}, 5000);
recordEvent("purchases", {
  item: "Avocado",
  price: 3,
  number_of_items: 10,
  user: {
    name: "diaa",
  },
});
recordEvent("pageviews", {
  ip_address: "${keen.ip}",
  user_agent: "${keen.user_agent}",
  location: "${keen.location}",
  uniqueness_token: "${keen.uniqueness_token}",
});
recordEvent("logins", {
  ip_address: "${keen.ip}",
  user_agent: "${keen.user_agent}",
});

recordEvent("logout", {
  ip_address: "${keen.ip}",
  keen: {
    addons: [
      {
        name: "keen:ip_to_geo",
        input: {
          ip: "ip_address",
        },
        output: "ip_geo_info",
      },
    ],
  },
});

recordEvent("checker", {
  user_agent: "${keen.user_agent}",
  keen: {
    addons: [
      {
        name: "keen:ua_parser",
        input: {
          ua_string: "user_agent",
        },
        output: "parsed_user_agent",
      },
    ],
  },
});

recordEvent("referrer", {
  referrer: {
    url: "https://search-engine.com?search=analytics",
  },
  page: {
    url: "http://mysite.com/landing-page",
  },
  keen: {
    addons: [
      {
        name: "keen:referrer_parser",
        input: {
          referrer_url: "referrer.url",
          page_url: "page.url",
        },
        output: "referrer.info",
      },
    ],
  },
});
// console.log(IntersectionObserver);

if (IntersectionObserver !== undefined) {
  let option = {
    threshold: 1.0,
  };

  function callback(entries, observer) {
    console.log("hello from observer intrersection", entries, observer);
  }
  const observer = new IntersectionObserver(callback, option);
  let manualElements = document.querySelectorAll(".manual-tracking");
  manualElements.forEach(function (element) {
    observer.observe(element);
  });
}
const btn = document
  .querySelector("#add-route")
  .addEventListener("click", function (e) {
    recordEvent("btn_clicked", {
      ip_address: "${keen.ip}",
      user_agent: "${keen.user_agent}",
      page_url: document.location.href,
      referrer: {
        url: document.location.href,
      },
      page: {
        url: document.referrer,
      },
      keen: {
        addons: [
          {
            name: "keen:ip_to_geo",
            input: {
              ip: "ip_address",
            },
            output: "ip_geo_info",
          },
          {
            name: "keen:ua_parser",
            input: {
              ua_string: "user_agent",
            },
            output: "parsed_user_agent",
          },
          {
            name: "keen:date_time_parser",
            input: {
              date_time: "keen.timestamp",
            },
            output: "timestamp_info",
          },
          {
            name: "keen:url_parser",
            input: {
              url: "page_url",
            },
            output: "parsed_page_url",
          },
          {
            name: "keen:referrer_parser",
            input: {
              referrer_url: "referrer.url",
              page_url: "page.url",
            },
            output: "referrer.info",
          },
        ],
      },
    });
  });

queryEventForAnalysis();

// window.onload=function() {
//   if(location.)

console.log(this);
{
  console.log(this, "this");
}
